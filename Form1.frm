VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  '固定(実線)
   Caption         =   "特殊加工Ｂ"
   ClientHeight    =   4515
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5715
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4515
   ScaleWidth      =   5715
   StartUpPosition =   2  '画面の中央
   Begin VB.Frame Frame1 
      Caption         =   "フィルタ"
      Height          =   1140
      Left            =   150
      TabIndex        =   8
      Top             =   2250
      Width           =   5415
      Begin VB.HScrollBar HScroll1 
         Height          =   240
         Left            =   180
         Max             =   255
         TabIndex        =   13
         Top             =   705
         Value           =   127
         Width           =   4395
      End
      Begin VB.OptionButton Option1 
         Caption         =   "エンボス効果"
         Height          =   315
         Index           =   3
         Left            =   3855
         TabIndex        =   12
         Top             =   315
         Width           =   1425
      End
      Begin VB.OptionButton Option1 
         Caption         =   "エッジ抽出"
         Height          =   315
         Index           =   2
         Left            =   2490
         TabIndex        =   11
         Top             =   315
         Width           =   1200
      End
      Begin VB.OptionButton Option1 
         Caption         =   "シャープ"
         Height          =   315
         Index           =   1
         Left            =   1215
         TabIndex        =   10
         Top             =   315
         Width           =   1050
      End
      Begin VB.OptionButton Option1 
         Caption         =   "ぼかし"
         Height          =   315
         Index           =   0
         Left            =   165
         TabIndex        =   9
         Top             =   315
         Value           =   -1  'True
         Width           =   915
      End
      Begin VB.Label Label2 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "127"
         Height          =   255
         Left            =   4695
         TabIndex        =   14
         Top             =   705
         Width           =   435
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5130
      Top             =   3735
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
      DefaultExt      =   "BMP"
      Filter          =   "ﾋﾞｯﾄﾏｯﾌﾟ|*.bmp"
      Flags           =   6
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   225
      Left            =   135
      TabIndex        =   7
      Top             =   4140
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   397
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.CommandButton Command3 
      Caption         =   "セーブ"
      Enabled         =   0   'False
      Height          =   480
      Left            =   3840
      TabIndex        =   6
      Top             =   3510
      Width           =   1710
   End
   Begin VB.CommandButton Command2 
      Caption         =   "変換"
      Enabled         =   0   'False
      Height          =   480
      Left            =   2025
      TabIndex        =   5
      Top             =   3510
      Width           =   1710
   End
   Begin VB.CommandButton Command1 
      Caption         =   "ロード"
      Enabled         =   0   'False
      Height          =   480
      Left            =   135
      TabIndex        =   4
      Top             =   3510
      Width           =   1710
   End
   Begin VB.FileListBox File1 
      Height          =   1710
      Left            =   3000
      Pattern         =   "*.bmp"
      TabIndex        =   2
      Top             =   105
      Width           =   2565
   End
   Begin VB.DirListBox Dir1 
      Height          =   1350
      Left            =   135
      TabIndex        =   1
      Top             =   465
      Width           =   2790
   End
   Begin VB.DriveListBox Drive1 
      Height          =   300
      Left            =   135
      TabIndex        =   0
      Top             =   120
      Width           =   2790
   End
   Begin VB.Label Label1 
      BorderStyle     =   1  '実線
      Height          =   270
      Left            =   150
      TabIndex        =   3
      Top             =   1935
      Width           =   5415
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim op As Integer

Private Sub Command1_Click()
    Dim w As Integer
    Dim h As Integer
    Dim f As String
    Dim p As String
    
    p = File1.Path
    f = File1.FileName
    If Right$(p, 1) <> "\" Then p = p + "\"
    CommonDialog1.InitDir = p
    CommonDialog1.FileName = Mid$(f, 1, Len(f) - 4) + "_.bmp"

    f = Label1.Caption
    
    Form2.Picture1.Picture = LoadPicture(f)
    Form2.Picture1.Refresh
    
    w = Form2.Picture1.Width
    h = Form2.Picture1.Height
    
    Form2.Top = 0
    Form2.Left = 0
    Form2.Width = w + 90
    Form2.Height = h + 375

    Form2.Show
    
    Command2.Enabled = True
    
    Form1.SetFocus
    
End Sub

Private Sub Command2_Click()
    Dim mtrx(2, 2) As Double
    Dim i As Integer, j As Integer
    Dim m As Integer, n As Integer
    Dim r As Double, g As Double, b As Double
    Dim c As Long
    Dim pt As Double
    Dim a1 As Double, a2 As Double
    Dim h As Integer, w As Integer
    Dim X As Integer, Y As Integer
    Dim k As Double
    Dim br As Double
   
    Drive1.Enabled = False
    Dir1.Enabled = False
    File1.Enabled = False
    Option1(0).Enabled = False
    Option1(1).Enabled = False
    Option1(2).Enabled = False
    Option1(3).Enabled = False
    HScroll1.Enabled = False
    Command1.Enabled = False
    Command2.Enabled = False
    Command3.Enabled = False
   
    Form3.Top = 400
    Form3.Left = 400
    Form3.Width = Form2.Width
    Form3.Height = Form2.Height
    Form3.Picture1.Width = Form2.Picture1.Width
    Form3.Picture1.Height = Form2.Picture1.Height
    
    Select Case op
    Case 0
        mtrx(0, 0) = 1# / 9#: mtrx(0, 1) = 1# / 9#: mtrx(0, 2) = 1# / 9#
        mtrx(1, 0) = 1# / 9#: mtrx(1, 1) = 1# / 9#: mtrx(1, 2) = 1# / 9#
        mtrx(2, 0) = 1# / 9#: mtrx(2, 1) = 1# / 9#: mtrx(2, 2) = 1# / 9#
        br = 0#
    Case 1
        mtrx(0, 0) = 0#:  mtrx(0, 1) = -1#: mtrx(0, 2) = 0#
        mtrx(1, 0) = -1#: mtrx(1, 1) = 5#:  mtrx(1, 2) = -1#
        mtrx(2, 0) = 0#:  mtrx(2, 1) = -1#: mtrx(2, 2) = 0#
        br = 0#
    Case 2
        mtrx(0, 0) = 0#: mtrx(0, 1) = 0#: mtrx(0, 2) = 0#
        mtrx(1, 0) = 0#: mtrx(1, 1) = 1#: mtrx(1, 2) = 0#
        mtrx(2, 0) = 0#: mtrx(2, 1) = 0#: mtrx(2, 2) = -1#
        br = 0#
    Case 3
        mtrx(0, 0) = 0#: mtrx(0, 1) = 0#: mtrx(0, 2) = 0#
        mtrx(1, 0) = 0#: mtrx(1, 1) = 1#: mtrx(1, 2) = 0#
        mtrx(2, 0) = 0#: mtrx(2, 1) = 0#: mtrx(2, 2) = -1#
        br = CDbl(HScroll1.Value)
    Case Else
    End Select
    
    w = CInt(Form2.Picture1.ScaleWidth)
    h = CInt(Form2.Picture1.ScaleHeight)
    Form2.Picture1.Line (-1, -1)-(w, h), vbBlack, B
    a1 = CDbl(w) * CDbl(h)
    
    For i = 0 To h - 1
        For j = 0 To w - 1
            c = 0
            r = 0
            g = 0
            b = 0
            For m = 0 To 2
                For n = 0 To 2
                    X = j + n - 1
                    Y = i + m - 1
                    k = mtrx(m, n)
                    pt = CDbl(Form2.Picture1.Point(X, Y))
                    b = b + k * (pt \ (256# * 256#))
                    g = g + k * ((pt Mod (256# * 256#)) \ 256#)
                    r = r + k * ((pt Mod (256# * 256#)) Mod 256#)
                Next n
            Next m
            r = r + br
            g = g + br
            b = b + br
            If r < 0# Then r = 0#
            If g < 0# Then g = 0#
            If b < 0# Then b = 0#
            If r > 255# Then r = 255#
            If g > 255# Then g = 255#
            If b > 255# Then b = 255#
            c = RGB(CInt(r), CInt(g), CInt(b))
            Form3.Picture1.PSet (j, i), c
            a2 = CDbl(i) * CDbl(w) + CDbl(j)
            ProgressBar1.Value = CSng(100# * a2 / a1)
        Next j
    Next i
    
    ProgressBar1.Value = 100
    
    Form3.Show
    
    Command3.Enabled = True
    
    Drive1.Enabled = True
    Dir1.Enabled = True
    File1.Enabled = True
    Option1(0).Enabled = True
    Option1(1).Enabled = True
    Option1(2).Enabled = True
    Option1(3).Enabled = True
    If op = 3 Then
        HScroll1.Enabled = True
    End If
    Command1.Enabled = True
    Command2.Enabled = True
    
    Form1.SetFocus
    
End Sub

Private Sub Command3_Click()
    Dim p As String
    
    On Error GoTo Command3_Error
    CommonDialog1.ShowSave
    p = CommonDialog1.FileName
    SavePicture Form3.Picture1.Image, p
    File1.Refresh
    Exit Sub
Command3_Error:
    Dim e As String
    If Err = cdlCancel Then
        Exit Sub
    Else
        e = "ｴﾗｰ " + Str(Err) + Chr$(13) + Error(Err)
        MsgBox e, , "ｴﾗｰ"
        Stop
    End If
End Sub

Private Sub Dir1_Change()
    File1.Path = Dir1.Path
    Label1.Caption = ""
    Command1.Enabled = False
End Sub

Private Sub Drive1_Change()
    Dir1.Path = Drive1.Drive
End Sub

Private Sub File1_Click()
    Dim f As String
    Dim p As String
    p = File1.Path
    f = File1.FileName
    If Right$(p, 1) <> "\" Then p = p + "\"
    Label1.Caption = p + f
    Command1.Enabled = True
End Sub

Private Sub Form_Load()
    op = 0
    HScroll1.Enabled = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Unload Form2
    Unload Form3
End Sub


Private Sub HScroll1_Change()
    Label2.Caption = Str(HScroll1.Value)
End Sub

Private Sub Option1_Click(Index As Integer)
    op = Index
    If op = 3 Then
        HScroll1.Enabled = True
    Else
        HScroll1.Enabled = False
    End If
End Sub
